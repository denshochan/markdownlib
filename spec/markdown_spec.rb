# coding: utf-8
require 'spec_helper'

describe Markdownlib::Markdown do
  before :each do
    @markdown = Markdownlib::Markdown.new()
  end

  it 'UTF-8 BOM should be removed' do
    expected = "\n"
    actual = @markdown.transform("\xEF\xBB\xBF")

    expect(actual).to eq(expected)
  end

  it 'marker character should be removed' do
    expected = "\n"
    actual = @markdown.transform("\x1A")

    expect(actual).to eq(expected)
  end

  it 'line endings should be standerized' do
    expected = "\n"
    actual = @markdown.transform("\r\n\r")

    expect(actual).to eq(expected)
  end

  it 'tabs should be replaced by the appropriate amount of space.' do
    expected = "<pre><code>foo     bar\n</code></pre>\n"
    actual = @markdown.transform("\tfoo\t\tbar")

    expect(actual).to eq(expected)
  end

  it 'strip_link_definitions' do
    expected = "\n"
    actual = @markdown.send(:strip_link_definitions, "\n [id]: <http://example.com/>  \"Optional Title Here\"")
    expect(actual).to eq(expected)

    expected = "\nHere's a [link] [1] with an ampersand in the URL.\n\n"
    actual = @markdown.send(:strip_link_definitions, "\nHere's a [link] [1] with an ampersand in the URL.\n\n[1]: http://example.com/?foo=1&bar=2")
    expect(actual).to eq(expected)

    expected = "\nHere's a [link] [1] with an ampersand in the URL.\n\n"
    actual = @markdown.send(:strip_link_definitions, "\nHere's a [link] [1] with an ampersand in the URL.\n\n[1]: http://example.com/?foo=1&bar=2\n\n[2]: http://att.com/  \"AT&T\"")
    expect(actual).to eq(expected)

  end

  it 'do_italics_and_bold' do
    @markdown.send(:prepare_italics_and_bold)

    key = @markdown.send(:do_italics_and_bold, "*text*\n").gsub!(/\n/,'')
    actual = @markdown.send(:unhash, key)
    expected = "<em>text</em>"
    expect(actual).to eq(expected)

    key = @markdown.send(:do_italics_and_bold, "**text**\n").gsub!(/\n/,'')
    actual = @markdown.send(:unhash, key)
    expected = "<strong>text</strong>"
    expect(actual).to eq(expected)

    key = @markdown.send(:do_italics_and_bold, "***text***\n").gsub!(/\n/,'')
    actual = @markdown.send(:unhash, key)
    expected = "<strong><em>text</em></strong>"
    expect(actual).to eq(expected)

    key = @markdown.send(:do_italics_and_bold, "_text_\n").gsub!(/\n/,'')
    actual = @markdown.send(:unhash, key)
    expected = "<em>text</em>"
    expect(actual).to eq(expected)

    key = @markdown.send(:do_italics_and_bold, "__text__\n").gsub!(/\n/,'')
    actual = @markdown.send(:unhash, key)
    expected = "<strong>text</strong>"
    expect(actual).to eq(expected)

    key = @markdown.send(:do_italics_and_bold, "___text___\n").gsub!(/\n/,'')
    actual = @markdown.send(:unhash, key)
    expected = "<strong><em>text</em></strong>"
    expect(actual).to eq(expected)
  end

  it 'do_block_quotes' do

    key = @markdown.send(:do_block_quotes, "> text\n").gsub!(/\n/,'')
    actual = @markdown.send(:unhash, key)
    expected = "<blockquote>\n  <p>text</p>\n</blockquote>"
    expect(actual).to eq(expected)

    key = @markdown.send(:do_block_quotes, "> <pre>text</pre>\n").gsub!(/\n/,'')
    actual = @markdown.send(:unhash, key)
    expected = "<blockquote>\n<pre>text</pre>\n</blockquote>"
    expect(actual).to eq(expected)

    key = @markdown.send(:do_block_quotes, ">   text\n").gsub!(/\n/,'')
    actual = @markdown.send(:unhash, key)
    expected = "<blockquote>\n  <p>text</p>\n</blockquote>"
    expect(actual).to eq(expected)
  end

  it 'do_headers setex' do
    index = 1
    expected = "<h1>header1</h1>"
    @markdown.send(:do_headers, "header1\n=======\n")
    actual = @markdown.send(:unhash, "B\u001A#{index}B")
    expect(actual).to eq(expected)

    index += 1
    expected = "<h2>header2</h2>"
    @markdown.send(:do_headers, "header2\n-------\n")
    actual = @markdown.send(:unhash, "B\u001A#{index}B")
    expect(actual).to eq(expected)

    expected = "-\n-\n"
    actual = @markdown.send(:do_headers, "-\n-\n")
    expect(actual).to eq(expected)
  end

  it 'do_headers atx' do
    index = 1
    expected = "<h1>header1</h1>"
    @markdown.send(:do_headers, "# header1\n")
    actual = @markdown.send(:unhash, "B\u001A#{index}B")
    expect(actual).to eq(expected)

    index += 1
    expected = "<h2>header2</h2>"
    @markdown.send(:do_headers, "## header2\n")
    actual = @markdown.send(:unhash, "B\u001A#{index}B")
    expect(actual).to eq(expected)

    index += 1
    expected = "<h3>header3</h3>"
    @markdown.send(:do_headers, "### header3\n")
    actual = @markdown.send(:unhash, "B\u001A#{index}B")
    expect(actual).to eq(expected)

    index += 1
    expected = "<h4>header4</h4>"
    @markdown.send(:do_headers, "#### header4\n")
    actual = @markdown.send(:unhash, "B\u001A#{index}B")
    expect(actual).to eq(expected)

    index += 1
    expected = "<h5>header5</h5>"
    @markdown.send(:do_headers, "##### header5\n")
    actual = @markdown.send(:unhash, "B\u001A#{index}B")
    expect(actual).to eq(expected)

    index += 1
    expected = "<h6>header6</h6>"
    @markdown.send(:do_headers, "###### header6\n")
    actual = @markdown.send(:unhash, "B\u001A#{index}B")
    expect(actual).to eq(expected)

  end

  it 'do_lists' do
    text = <<"EOS"

- one
- two
- three
    1. one
    2. two

EOS
    expected = "<ul>\n<li>one</li>\n<li>two</li>\n<li>three\n\n<ol>\n<li>one</li>\n<li>two</li>\n</ol></li>\n</ul>"
    key = @markdown.send(:do_lists, text).gsub!(/\n/,'')
    actual = @markdown.send(:unhash, key)

    expect(actual).to eq(expected)

    text = <<"EOS"

1. one
2. two
3. three
    - one
    - two

EOS
    expected = "<ol>\n<li>one</li>\n<li>two</li>\n<li>three\n\n<ul>\n<li>one</li>\n<li>two</li>\n</ul></li>\n</ol>"
    key = @markdown.send(:do_lists, text).gsub!(/\n/,'')
    actual = @markdown.send(:unhash, key)

    expect(actual).to eq(expected)

  end

  it 'process_list_items' do
    marker_any_re = "(?:[*+-]|\d+[\.])"
    expected = "<li>one</li>\n"
    actual = @markdown.send(:process_list_items, "- one\n\n", marker_any_re)

    expect(actual).to eq(expected)
  end

  it 'do_horizontal_rules' do
    expected = "text"
    actual = @markdown.send(:do_horizontal_rules, "text")
    expect(actual).to eq(expected)

    expected = "\n\n<hr />\n\n"
    key = @markdown.send(:do_horizontal_rules, "\n---\n").gsub('\n','')
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)

    expected = "\n\n<hr />\n\n"
    key = @markdown.send(:do_horizontal_rules, "\n- - -\n").gsub('\n','')
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)

    expected = "\n\n<hr />\n\n"
    key = @markdown.send(:do_horizontal_rules, "\n***\n").gsub('\n','')
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)

    expected = "\n\n<hr />\n\n"
    key = @markdown.send(:do_horizontal_rules, "\n* * *\n").gsub('\n','')
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)

    expected = "\n\n<hr />\n\n"
    key = @markdown.send(:do_horizontal_rules, "\n___\n").gsub('\n','')
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)

    expected = "\n\n<hr />\n\n"
    key = @markdown.send(:do_horizontal_rules, "\n_ _ _\n").gsub('\n','')
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)
  end

  it 'do_hard_breaks' do
    expected = "lorem<br />\nipsum"
    key = @markdown.send(:do_hard_breaks, "lorem  \nipsum")
    actual = @markdown.send(:unhash, key)

    expect(actual).to eq(expected)
  end

  it 'do_code_blocks' do
    expected = "<pre><code>&lt;div&gt;\nHello\n&lt;/div&gt;\n</code></pre>"
    key = @markdown.send(:do_code_blocks, "\n    <div>\n    Hello\n    </div>\n\n").gsub!(/\n/, '')
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)
  end

  it 'make_code_span' do
    expected = "<code>&amp;&lt;&gt;\"\'<\/code>"
    key = @markdown.send(:make_code_span, "&<>\"\'")
    actual = @markdown.send(:unhash, key)

    expect(actual).to eq(expected)
  end

  it 'hash_part' do
    expected = "x\x1A1x"
    actual = @markdown.send(:hash_part, "text")

    expect(actual).to eq(expected)

    expected = "B\x1A2B"
    actual = @markdown.send(:hash_part, "text", "B")

    expect(actual).to eq(expected)
  end

  it 'do_anchors' do

    expected = "<a href=\"http://example1.com/\">link text</a>"
    @markdown.send(:strip_link_definitions, "\n [id1]: <http://example1.com/>")
    key = @markdown.send(:do_anchors, "[link text][id1]")
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)

    expected = "<a href=\"http://example2.com/\">link text</a>"
    key = @markdown.send(:do_anchors, "[link text](http://example2.com/)")
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)

    expected = "<a href=\"http://example3.com/\" title=\"Optional Title Here\">link text</a>"
    key = @markdown.send(:do_anchors, "[link text](http://example3.com/ \"Optional Title Here\")")
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)

    expected = "<a href=\"http://example4.com/\">link text</a>"
    @markdown.send(:strip_link_definitions, "\n [link text]: <http://example4.com/>")
    key = @markdown.send(:do_anchors, "[link text]")
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)

    expected = "<a href=\"/foo\">link text</a>"
    key = @markdown.send(:do_anchors, "[link text](/foo)")
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)

  end

  it 'do_images' do

    expected = "<img src=\"http://example1.com/\" alt=\"alt text\" />"
    @markdown.send(:strip_link_definitions, "\n [id1]: <http://example1.com/>")
    key = @markdown.send(:do_images, "![alt text][id1]")
    actual = @markdown.send(:unhash, key)
    expect(actual).to eq(expected)

    expected = "<img src=\"http://example2.com/\" alt=\"alt text\" title=\"Optional Title Here\" />"
    @markdown.send(:strip_link_definitions, "\n [id2]: <http://example2.com/>  \"Optional Title Here\"")
    key = @markdown.send(:do_images, "![alt text][id2]")
    actual = @markdown.send(:unhash, key)

    expect(actual).to eq(expected)

    expected = "<img src=\"http://example3.com/\" alt=\"alt text\" />"
    key = @markdown.send(:do_images, "![alt text](http://example3.com/)")
    actual = @markdown.send(:unhash, key)

    expect(actual).to eq(expected)

    expected = "<img src=\"http://example4.com/\" alt=\"alt text\" title=\"Optional Title Here\" />"
    key = @markdown.send(:do_images, "![alt text](http://example4.com/ \"Optional Title Here\")")
    actual = @markdown.send(:unhash, key)

    expect(actual).to eq(expected)

  end

  it 'detab' do
    expected = "text"
    actual = @markdown.send(:detab, "text")

    expect(actual).to eq(expected)

    expected = "    text"
    actual = @markdown.send(:detab, "\ttext")

    expect(actual).to eq(expected)

    expected = "    tex t"
    actual = @markdown.send(:detab, "\ttex\tt")

    expect(actual).to eq(expected)

    expected = "    te  xt"
    actual = @markdown.send(:detab, "\tte\txt")

    expect(actual).to eq(expected)

      expected = "    t   ext"
    actual = @markdown.send(:detab, "\tt\text")

    expect(actual).to eq(expected)

    expected = "    one\n\n    two\n\n    three\n\n"
    actual = @markdown.send(:detab, "\tone\n\n\ttwo\n\n\tthree\n\n")
    expect(actual).to eq(expected)
  end

  it 'encode_attribute' do
    expected = "&amp;&lt;&quot;"
    actual = @markdown.send(:encode_attribute, "&<\"")

    expect(actual).to eq(expected)
  end

  it 'form_paragraphs' do

    expected = "<p>text</p>"
    actual = @markdown.send(:form_paragraphs, "text")
    expect(actual).to eq(expected)

    expected = "<p>text</p>\n\n<p>text</p>"
    actual = @markdown.send(:form_paragraphs, "text\n\ntext")
    expect(actual).to eq(expected)

    expected = "<p>text\ntext</p>"
    actual = @markdown.send(:form_paragraphs, "text\ntext")
    expect(actual).to eq(expected)

  end

  it 'encode_amps_and_angles' do
    expected = "&amp;&lt;"
    actual = @markdown.send(:encode_amps_and_angles, "&<")

    expect(actual).to eq(expected)

    @markdown.no_entities = true
    expected = "&amp;&lt;"
    actual = @markdown.send(:encode_amps_and_angles, "&<")

    expect(actual).to eq(expected)

    @markdown.no_entities = false
    expected = "&amp;#;&lt;"
    actual = @markdown.send(:encode_amps_and_angles, "&#;<")

    expect(actual).to eq(expected)
  end

  it 'do_auto_links' do
    key = @markdown.send(:do_auto_links, "<http://example.com>")
    actual = @markdown.send(:unhash, key)
    expected = "<a href=\"http://example.com\">http://example.com</a>"
    expect(actual).to eq(expected)

    key = @markdown.send(:do_auto_links, "<foo@example.com>")
    actual = @markdown.send(:unhash, key)
    expected = '<a href="&#x6d;&#97;&#105;&#x6c;&#116;&#111;&#x3a;&#x66;&#111;&#x6f;&#x40;&#101;&#120;&#x61;&#109;&#112;&#x6c;&#x65;&#46;&#x63;&#x6f;&#109;">&#x66;&#111;&#x6f;&#x40;&#101;&#120;&#x61;&#109;&#112;&#x6c;&#x65;&#46;&#x63;&#x6f;&#109;</a>'
    expect(actual).to eq(expected)
  end

  it 'encode_email_address' do
    expected = '<a href="&#x6d;&#97;&#105;&#x6c;&#116;&#111;&#x3a;&#x66;&#111;&#x6f;&#x40;&#101;&#120;&#x61;&#109;&#112;&#x6c;&#x65;&#46;&#x63;&#x6f;&#109;">&#x66;&#111;&#x6f;&#x40;&#101;&#120;&#x61;&#109;&#112;&#x6c;&#x65;&#46;&#x63;&#x6f;&#109;</a>'
    actual = @markdown.send(:encode_email_address, "foo@example.com")

    expect(actual).to eq(expected)
  end
 
  it 'parse_span' do
    str = "lorem<!-- comment -->ipsum<? instruction ?>dolor<% instruction %>sit<tag attr=\"value\">amet, <tag />consectetur</tag>"

    expected = "loremx\u001A1xipsumx\u001A2xdolorx\u001A3xsitx\u001A4xamet, x\u001A5xconsecteturx\u001A6x"
    actual = @markdown.send(:parse_span, str)

    expect(actual).to eq(expected)

    @markdown.no_markup = true
    expected = str
    actual = @markdown.send(:parse_span, str)
    expect(actual).to eq(expected)

  end

  it 'handle_span_token' do
    expected = "&#116;"
    data = @markdown.send(:handle_span_token, "\\text", "text")
    actual = @markdown.send(:unhash, data[:ret])
    expect(actual).to eq(expected)

    expected = "<code>t</code>"
    data = @markdown.send(:handle_span_token, "`text`", "t`text`")
    actual = @markdown.send(:unhash, data[:ret])
    expect(actual).to eq(expected)

    expected = "`text`"
    data = @markdown.send(:handle_span_token, "`text`", "text")
    actual = @markdown.send(:unhash, data[:ret])
    expect(actual).to eq(expected)

    expected = "text"
    data = @markdown.send(:handle_span_token, "text", "text")
    actual = @markdown.send(:unhash, data[:ret])
    expect(actual).to eq(expected)
  end

  it 'outdent' do
    expected = "text"
    actual = @markdown.send(:outdent, "\ttext")

    expect(actual).to eq(expected)

    expected = "text"
    actual = @markdown.send(:outdent, " text")

    expect(actual).to eq(expected)

    expected = "text"
    actual = @markdown.send(:outdent, "  text")

    expect(actual).to eq(expected)

    expected = "text"
    actual = @markdown.send(:outdent, "   text")

    expect(actual).to eq(expected)

    expected = "text"
    actual = @markdown.send(:outdent, "    text")

    expect(actual).to eq(expected)

    expected = " text"
    actual = @markdown.send(:outdent, "     text")

    expect(actual).to eq(expected)
  end

  it 'hash_block' do
    expected = "B\u001A1B"
    actual = @markdown.send(:hash_block, "text")

    expect(actual).to eq(expected)
  end

  it 'unhash' do
    expected = "text"
    actual = @markdown.send(:unhash, "text")

    expect(actual).to eq(expected)

    key = @markdown.send(:hash_part, "text")
    actual = @markdown.send(:unhash, key)

    expect(actual).to eq(expected)
  end

  it 'Japanese Text' do
    expected = "<p>いろはにほへと</p>\n"
    actual = @markdown.transform("いろはにほへと")

    expect(actual).to eq(expected)
  end


  testdir = __dir__ + "/PHPMarkdownTest_1.3/Tests"
  Dir.glob(testdir + '/*.text').each do | file |
    casename = File.basename(file, ".text")
    html = testdir + '/' + casename + ".html"
    it "MarkdownTest_1.0.3: #{casename}" do
      input_handler = open(file)
      actual = @markdown.transform(input_handler.read)
      expected_handler = open(html)
      expected = expected_handler.read
      expect(actual).to eq(expected)
    end
  end

end