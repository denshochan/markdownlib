# coding: utf-8

module Markdownlib
  
  class Markdown
  
    ### Accessors ###
    attr_accessor :empty_element_suffix, :tab_width
    attr_accessor :no_markup, :no_entities
    attr_accessor :predef_urls, :predef_titles
  
    ### Version ###
  
    MARKDOWNLIB_VERSION  =  "1.3"
  
    def initialize
    #
    # Constructor function. Initialize appropriate member variables.
    #
      
      @i = 0

      ### Configuration Variables ###
  
      # Change to ">" for HTML output.
      @empty_element_suffix = " />"
      @tab_width = 4
  
      # Change to `true` to disallow markup or entities.
      @no_markup = false
      @no_entities = false
  
      # Predefined urls and titles for reference links and images.
      @predef_urls = {}
      @predef_titles = {}
  
      ### Parser Implementation ###
  
      @nested_brackets_depth = 6
      @nested_brackets_re
  
      @nested_url_parenthesis_depth = 4
      @nested_url_parenthesis_re
  
      # Table of hash values for escaped characters:
      @escape_chars = '\`*_{}[]()>#+-.!'
      @escape_chars_re
  
      @em_relist = {
        '' => '(?:(?<!\*)\*(?!\*)|(?<!_)_(?!_))(?=\S|$)(?![\.,:;]\s)',
        '*' => '(?<=\S|^)(?<!\*)\*(?!\*)',
        '_' => '(?<=\S|^)(?<!_)_(?!_)',
      }
  
      @strong_relist = {
        '' => '(?:(?<!\*)\*\*(?!\*)|(?<!_)__(?!_))(?=\S|$)(?![\.,:;]\s)',
        '**' => '(?<=\S|^)(?<!\*)\*\*(?!\*)',
        '__' => '(?<=\S|^)(?<!_)__(?!_)',
      }
  
      @em_strong_relist = {
        '' => '(?:(?<!\*)\*\*\*(?!\*)|(?<!_)___(?!_))(?=\S|$)(?![\.,:;]\s)',
        '***' => '(?<=\S|^)(?<!\*)\*\*\*(?!\*)',
        '___' => '(?<=\S|^)(?<!_)___(?!_)',
      }
  
      @em_strong_prepared_relist = {}
  
      prepare_italics_and_bold()
  
      @nested_brackets_re = '(?>[^\[\]]+|\[' * @nested_brackets_depth + '\])*' * @nested_brackets_depth
      @nested_url_parenthesis_re = '(?>[^()\s]+|\(' * @nested_url_parenthesis_depth + '(?>\)))*' * @nested_url_parenthesis_depth
      @escape_chars_re = '[' + Regexp.escape(@escape_chars) + ']'
  
      # Sort document, block, and span gamut in ascendent priority order.
      asort(document_gamut)
      asort(block_gamut)
      asort(span_gamut)
  
      # Internal hashes used during transformation.
      @urls = {}
      @titles = {}
      @html_hashes = {}
  
      # Status flag to avoid invalid nesting.
      @in_anchor = false

      @list_level = 0
    end

    private
    #
    # Called before the transformation process starts to setup parser 
    # states.
    #
    def setup()
      # Clear global hashes.
      @urls = @predef_urls
      @titles = @predef_titles
      @html_hashes = {}
      @in_anchor = false;
    end

    private
    #
    # Called after the transformation process to clear any variable 
    # which may be taking up memory unnecessarly.
    #  
    def teardown()
      @urls = {}
      @titles = {}
      @html_hashes = {}
    end

    public
    #
    # Main function. Performs some preprocessing on the input text
    # and pass it through the document gamut.
    #
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def transform(text)
      setup()
  
      # Remove UTF-8 BOM and marker character in input, if present.
      text.sub!(%r{^\xEF\xBB\xBF|\x1A}, '')
    
      # Standardize line endings:
      #   DOS to Unix and Mac to Unix
      text.gsub!(%r{\r\n?}, "\n")
    
      # Make sure $text ends with a couple of newlines:
      text += "\n\n"
    
      # Convert all tabs to spaces.
      text = detab(text)

      # Turn block-level HTML blocks into hash entries
      text = hash_html_blocks(text);

      # Strip any lines consisting only of spaces and tabs.
      # This makes subsequent regexen easier to write, because we can
      # match consecutive blank lines with /\n+/ instead of something
      # contorted like /[ ]*\n+/ .
      text.gsub!(/^[ ]+$/m, '')

      # Run document gamut methods.
      document_gamut.each do | method, priority |
        text = send(method, text)
      end

      teardown()

      return text + "\n"
    end

    private
    # @return [Hash] @document_gamut
    def document_gamut
      @document_gamut ||= {
        # Strip link definitions, store in hashes.
        :strip_link_definitions  => 20,
        :run_basic_block_gamut   => 30
      }
      return @document_gamut
    end

    private
    #
    # Strips link definitions from text, stores the URLs and titles in
    # hash references.
    #
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def strip_link_definitions(text)
      less_than_tab = @tab_width -1

      # Link defs are in the form: ^[id]: url "optional title"
      text.gsub!(/^[ ]{0,#{less_than_tab}}\[(?<id>.+)\][ ]?:
        [ ]*
        \n?
        [ ]*
        (?:
            <(?<url>.+?)>
          |
            (?<url_alt>\S+?)
        )
          [ ]*
          \n?
          [ ]*
        (?:
          (?<=\s)
          ["(]
          (?<title>.*?)
          [")]
          [ ]*
        )?
        (?:\n+|\Z)
      /x) {|match| # PHP `m' descriptor is omitted
        _strip_link_definitions_callback($~)
      }
  
      return text
    end

    private
    # @param [String] matches Match result of _strip_link_definitions.
    # @return [String] Empty string.
    def _strip_link_definitions_callback(matches)
      link_id = matches[:id].downcase
      url = (matches[:url].nil? ? matches[:url_alt] : matches[:url])
      @urls[link_id] = url
      @titles[link_id] = matches[:title]
      return '' # String that will replace the block
    end

    private
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def hash_html_blocks(text) #need_refactoring
      if @no_markup
        return text
      end

      less_than_tab = @tab_width - 1

      # Hashify HTML blocks:
      # We only want to do this for block-level HTML tags, such as headers,
      # lists, and tables. That's because we still want to wrap <p>s around
      # "paragraphs" that are wrapped in non-block-level tags, such as anchors,
      # phrase emphasis, and spans. The list of tags we're looking for is
      # hard-coded:
      #
      # *  List "a" is made of tags which can be both inline or block-level.
      #    These will be treated block-level when the start tag is alone on 
      #    its line, otherwise they're not matched here and will be taken as 
      #    inline later.
      # *  List "b" is made of tags which are always block-level;
      #
      block_tags_a_re = 'ins|del'
      block_tags_b_re = 'p|div|h[1-6]|blockquote|pre|table|dl|ol|ul|address|script|noscript|form|fieldset|iframe|math|svg|article|section|nav|aside|hgroup|header|footer|figure'

      # Regular expression for the content of a block tag.
      nested_tags_level = 4
      attr = <<-EOS
        (?>       # optional tag attributes
          \s      # starts with whitespace
          (?>
          [^>"/]+   # text outside quotes
          |
          /+(?!>)   # slash not followed by ">"
          |
          "[^"]*"   # text inside double quotes (tolerate ">")
          |
          \'[^\']*\'  # text inside single quotes (tolerate ">")
          )*
        )?
      EOS

      content1_1 =<<-EOS * nested_tags_level
        (?>
          [^<]+      # content without tag
        |
          <\\k<start_tag>       # nested opening tag
            #{attr}  # attributes
            (?>
              />
            |
             >
      EOS

      content1_2 =<<-EOS * nested_tags_level
              </\\k<start_tag>\\s*> # closing nested tag
            )
        |
          <(?!/\\k<start_tag>\\s*>  # other tags with a different name
        )
       )*
      EOS

      content = content1_1 + '.*?' + content1_2
      content2 = content.gsub('start_tag', 'start_tag2')

      # First, look for nested blocks, e.g.:
      #   <div>
      #     <div>
      #     tags for inner block must be indented.
      #     </div>
      #   </div>
      #
      # The outermost tags must start at the left margin for this to match, and
      # the inner nested divs must be indented.
      # We need to do this before the next, more liberal match, because the next
      # match will start at the first `<div>` and stop at the first `</div>`.
      text.gsub!(/(?>
        (?>
            (?<=\n\n)   # Starting after a blank line
            |       # or
            \A\n?     # the beginning of the doc
        )
        (?<whole>
          [ ]{0,#{less_than_tab}}
          <(?<start_tag>#{block_tags_b_re})
          #{attr}>
          #{content}
          <\/\k<start_tag>>
          [ ]*
          (?=\n+|\Z)
          |
            [ ]{0,#{less_than_tab}}
            <(?<start_tag2>#{block_tags_a_re})
            #{attr}>[ ]*\n
            #{content2}
            <\/\k<start_tag2>>
          [ ]*
          (?=\n+|\Z)
          | # Special case just for <hr \/>. It was easier to make a special 
            # case than to make the other regex more complicated.
            [ ]{0,#{less_than_tab}}
            <(?<start_tag3>hr)
            #{attr}
            \/?>
            [ ]*
          (?=\n+|\Z)
          | # Special case for standalone HTML comments:
            [ ]{0,#{less_than_tab}}
            (?m:
              <!-- .*? -->
            )
            [ ]*
            (?=\n{2,}|\Z)   # followed by a blank line or end of document
          | # PHP and ASP-style processor instructions (<? and <%)
            [ ]{0,#{less_than_tab}}
            (?m:
              <(?<inst_char>[?%])     # $2
              .*?
              \k<inst_char>>
            )
            [ ]*
            (?=\n{2,}|\Z)   # followed by a blank line or end of document
         )
        )/xmi){|match|
        _hash_html_blocks_callback($~)
      }

      return text
    end

    private
    # @param [String] matches Match result of hash_html_blocks.
    # @return [String] Hashed HTML block.
    def _hash_html_blocks_callback(matches)
      matches
      text = matches[:whole]
      key  = hash_block(text)
    return "\n\n#{key}\n\n"
    end

    private
    #
    # Called whenever a tag must be hashed when a function insert an atomic 
    # element in the text stream. Passing $text to through this function gives
    # a unique text-token which will be reverted back when calling unhash.
    #
    # The boundary argument specify what character should be used to surround
    # the token. By convension, "B" is used for block elements that needs not
    # to be wrapped into paragraph tags at the end, ":" is used for elements
    # that are word separators and "X" is used in the general case.
    # @param [String] text text to be hashed
    # @return [String] String that will replace the tag.
    def hash_part(text, boundary = 'x')
      # Swap back any tag hash found in text so we do not have to `unhash`
      # multiple times at the end.
      text = unhash(text)

      # Then hash the block.
      @i += 1
      key = boundary + "\x1A" + @i.to_s + boundary
      @html_hashes[key] = text
      return key # String that will replace the tag.
    end

    private
    #
    # Shortcut method for hash_part with block-level boundaries.
    #
    # @param [String] text text to be hashed
    # @return [String] String that will replace the tag.
    def hash_block(text)
      return hash_part(text, 'B')
    end

    private
    #
    # These are all the transformations that form block-level
    # tags like paragraphs, headers, and list items.
    # 
    # @return [Hash] @block_gamut
    def block_gamut
      @block_gamut ||= {
        :do_headers          => 10,
        :do_horizontal_rules => 20,  
        :do_lists            => 40,
        :do_code_blocks      => 50,
        :do_block_quotes     => 60,
      }
      return @block_gamut
    end

    private
    #
    # Run block gamut tranformations.
    #
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def run_block_gamut(text)
      # We need to escape raw HTML in Markdown source before doing anything 
      # else. This need to be done for each block, and not only at the 
      # begining in the Markdown function since hashed blocks can be part of
      # list items and could have been indented. Indented blocks would have 
      # been seen as a code block in a previous pass of hash_html_blocks.
      text = hash_html_blocks(text)

      return run_basic_block_gamut(text)
    end

    private
    #
    # Run block gamut tranformations, without hashing HTML blocks. This is 
    # useful when HTML blocks are known to be already hashed, like in the first
    # whole-document pass.
    #
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def run_basic_block_gamut(text)
      block_gamut.each do | method, priority |
        text = send(method, text)
      end

      # Finally form paragraph and restore hashed blocks.
      text = form_paragraphs(text)

      return text
    end

    private
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def do_horizontal_rules(text)
      # Do Horizontal Rules:
      replacement = "\n" + hash_block('<hr' + @empty_element_suffix) + "\n"
      return text.gsub(
        %r{
        ^[ ]{0,3}   # Leading space
        ([-*_])     # $1: First marker
        (?>         # Repeated marker group
          [ ]{0,2}  # Zero, one, or two spaces.
          \1        # Marker character
        ){2,}       # Group repeated at least twice
        [ ]*        # Tailing spaces
        $           # End of line.
       }mx, replacement)
    end

    private
    #
    # Run span gamut tranformations.
    #
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def span_gamut
      @span_gamut ||= {
      #
      # These are all the transformations that occur *within* block-level
      # tags like paragraphs, headers, and list items.
      #
      # Process character escapes, code spans, and inline HTML
      # in one shot.
        :parse_span             => -30,
  
      # Process anchor and image tags. Images must come first,
      # because ![foo][f] looks like an anchor.
        :do_images              =>  10,
        :do_anchors             =>  20,
      
      # Make links out of things like `<http://example.com/>`
      # Must come after do_anchors, because you can use < and >
      # delimiters in inline links like [this](<url>).
        :do_auto_links          =>  30,
        :encode_amps_and_angles =>  40,
  
        :do_italics_and_bold    =>  50,
        :do_hard_breaks         =>  60,
      }
      return @span_gamut
    end

    private
    #
    # Run span gamut tranformations.
    #
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def run_span_gamut(text)
      span_gamut.each do | method, priority |
        text = send(method, text)
      end

      return text
    end

    private
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def do_hard_breaks(text)
      # Do hard breaks:
      text.gsub!(/ {2,}\n/) {|match|
        _do_hard_breaks_callback($~)
      }

      return text
    end

    private
    # @param [String] matches Match result of do_hard_breaks.
    # @return [String] hashed hard break tag.
    def _do_hard_breaks_callback(matches)
      return hash_part("<br" + @empty_element_suffix + "\n")
    end

    private
    #
    # Turn Markdown link shortcuts into XHTML <a> tags.
    #
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def do_anchors(text)
      if @in_anchor then
        return text
      end
      @in_anchor = true

      #
      # First, handle reference-style links: [link text] [id]
      #
      text.gsub!(%r{
        (         # wrap whole match in $1
          \[
          (#{@nested_brackets_re}) # link text = $2
          \]
  
          [ ]?        # one optional space
          (?:\n[ ]*)?   # one optional newline followed by spaces
  
          \[
          (.*?)   # id = $3
          \]
        )
        }xm){|match|
          _do_anchors_reference_callback($~)
      }

      #
      # Next, inline-style links: [link text](url "optional title")
      #
      text.gsub!(%r{
        (       # wrap whole match in $1
          \[
          (#{@nested_brackets_re}) # link text = $2
          \]
          \(      # literal paren
          [ \n]*
          (?:
            <(.+?)> # href = $3
          |
            (#{@nested_url_parenthesis_re})  # href = $4
          )
          [ \n]*
          (     # $5
            ([\'"]) # quote char = $6
            (.*?)   # Title = $7
            \6    # matching quote
            [ \n]*  # ignore any spaces/tabs between closing quote and )
          )?      # title is optional
          \)
        )
        }xm){|match|
          _do_anchors_inline_callback($~)
      }

      #
      # Last, handle reference-style shortcuts: [link text]
      # These must come last in case you've also got [link text][1]
      # or [link text](/foo)
      #
      text.gsub!(%r{
        (         # wrap whole match in $1
          \[
          ([^\[\]]+)    # link text = $2; can\'t contain [ or ]
          \]
        )
        }xm){|match|
        _do_anchors_reference_callback($~)
      }

      @in_anchor = false
      return text
    end

    private
    # @param [String] matches Match result of do_anchors.
    # @return [String] Hashed link tag.
    def _do_anchors_reference_callback(matches)
      whole_match =  matches[1]
      link_text   =  matches[2]
      link_id     = matches[3]

      if link_id.nil? || link_id.empty?
        # for shortcut links like [this][] or [this].
        link_id = link_text.dup
      end

      # lower-case and turn embedded newlines into spaces
      link_id.downcase!

      link_id.gsub!(%r{[ ]?\n}, ' ')

      if @urls[link_id]
        url = @urls[link_id]
        url = encode_attribute(url)

        result =  "<a href=\"#{url}\""
        if @titles[link_id]
          title = @titles[link_id]
          title = encode_attribute(title)
          result += " title=\"#{title}\""
        end

        link_text = run_span_gamut(link_text)
        result += ">#{link_text}</a>"
        result = hash_part(result)
      else
        result = whole_match
      end

      return result
    end

    private
    # @param [String] matches Match result of do_anchors.
    # @return [String] Hashed link tag.
    def _do_anchors_inline_callback(matches)
      whole_match =  matches[1]
      link_text =  run_span_gamut(matches[2])
      url =  matches[3].nil? ? matches[4] : matches[3]
      title = matches[7]

      url = encode_attribute(url)

      result =  "<a href=\"#{url}\""
      if title
        title = encode_attribute(title)
        result += " title=\"#{title}\""
      end

      link_text =  run_span_gamut(link_text)
      result += ">#{link_text}</a>"

      return hash_part(result)
    end

    private
    #
    # Turn Markdown image shortcuts into <img> tags.
    #
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def do_images(text)
      #
      # First, handle reference-style labeled images: ![alt text][id]
      #
      text.gsub!(%r{
        (                                # wrap whole match in $1
          !\[
            (#{@nested_brackets_re})                # alt text = $2
          \]

          [ ]?                                # one optional space
          (?:\n[ ]*)?                # one optional newline followed by spaces

          \[
            (.*?)                # id = $3
          \]

        )
        }xm){|match|
        _do_images_reference_callback($~)
      }

    #
    # Next, handle inline images:  ![alt text](url "optional title")
    # Don't forget: encode * and _
    #
      text.gsub!(%r{
        (       # wrap whole match in $1
        !\[
        (#{@nested_brackets_re})   # alt text = $2
        \]
        \s?     # One optional whitespace character
        \(      # literal paren
        [ \n]*
        (?:
          <(\S*)> # src url = $3
        |
          (#{@nested_url_parenthesis_re})  # src url = $4
        )
        [ \n]*
        (     # $5
          ([\'"]) # quote char = $6
          (.*?)   # title = $7
          \6    # matching quote
          [ \n]*
        )?      # title is optional
        \)
        )
        }xm){|match|
        _do_images_inline_callback($~)
      }

      return text
    end

    private
    # @param [String] matches Match result of do_images.
    # @return [String] Hashed img tag.
    def _do_images_reference_callback(matches)
      whole_match = matches[1]
      alt_text = matches[2]
      link_id = matches[3].downcase

      if link_id == ""
        link_id = alt_text.downcase
      end

      alt_text = encode_attribute(alt_text)
      if @urls[link_id]
        url = encode_attribute(@urls[link_id])
        result = "<img src=\"#{url}\" alt=\"#{alt_text}\""
        if @titles[link_id]
          title = encode_attribute(@titles[link_id])
          result += " title=\"#{title}\""
        end
        result += @empty_element_suffix
        result = hash_part(result)
      else
        # If there's no such link ID, leave intact:
        result = whole_match
      end

      return result
    end

    private
    # @param [String] matches Match result of do_images.
    # @return [String] Hashed img tag.
    def _do_images_inline_callback(matches)
      whole_match  = matches[1]
      alt_text   = matches[2]
      url  = matches[3].nil? ? matches[4] : matches[3]
      title = matches[7]

      alt_text = encode_attribute(alt_text)
      url = encode_attribute(url)
      result = "<img src=\"#{url}\" alt=\"#{alt_text}\""

      if title
        title = encode_attribute(title)
        result += " title=\"#{title}\"" # title already quoted
      end
      result += @empty_element_suffix

      return hash_part(result)
    end 

    private
    # @param [String] text text to be transformed..
    # @return [String] Text after transformation.
    def do_headers(text)
      # Setext-style headers:
      #          Header 1
      #          ========
      #  
      #          Header 2
      #          --------
      #
      text.gsub!(%r{ ^(.+?)[ ]*\n(=+|-+)[ ]*\n+ }x) {|match|
        _do_headers_callback_setext($~)
      }

      # atx-style headers:
      #        # Header 1
      #        ## Header 2
      #        ## Header 2 with closing hashes ##
      #        ...
      #        ###### Header 6
      #
      text.gsub!(%r{
         ^(\#{1,6})        # $1 = string of #\'s
         [ ]*
         (.+?)             # $2 = Header text
         [ ]*
         \#*               # optional closing #\'s (not counted)
         \n+
        }x) {|match|
        _do_headers_callback_atx($~)
      }

      return text
    end

    private
    # @param [String] matches Match result of Setext-style headers
    # @return [String] Hashed link tag.
    def _do_headers_callback_setext(matches)
      # Terrible hack to check we haven't found an empty list item.
      if matches[2] == '-' && matches[1].match(%r{^-(?: |$)})
        return matches[0]
      end

      if matches[2][0].chr == '='
        level = 1
      else
        level = 2
      end
      block = "<h#{level}>#{run_span_gamut(matches[1])}</h#{level}>"
      return "\n#{hash_block(block)}\n\n"
    end

    private
    # @param [String] matches Match result of atx-style headers
    # @return [String] Hashed link tag.
    def _do_headers_callback_atx(matches)
      level = matches[1].size
      block = "<h#{level}>#{run_span_gamut(matches[2])}</h#{level}>"
      return "\n#{hash_block(block)}\n\n"
    end

    private
    #
    # Form HTML ordered (numbered) and unordered (bulleted) lists.
    #
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def do_lists(text) #need_refactoring regexp 
      less_than_tab = @tab_width - 1

      # Re-usable patterns to match list item bullets and number markers:
      marker_ul_re  = '[*+-]'
      marker_ol_re  = '\d+[\.]'
      marker_any_re = "(?:#{marker_ul_re}|#{marker_ol_re})"

      markers_relist = {
        marker_ul_re => marker_ol_re,
        marker_ol_re => marker_ul_re,
      }

      markers_relist.each do |marker_re, other_marker_re|
        # Re-usable pattern to match any entirel ul or ol list:

        # We use a different prefix before nested lists than top-level lists.
        # See extended comment in _process_list_items().
        if @list_level != 0
          reg = /^(?<whole_list>
 (
   (?<spaces>[ ]{0,3})
   (?<marker>#{marker_re})
   [ ]+
 )
 (?m:.+?)
 (
  \z
   |
     \n{2,}
     (?=\S)
     (?!
      [ ]*#{marker_re}[ ]+
     )
   |
  (?=
    \n
    \k<spaces>#{other_marker_re}[ ]+
  )
 )
)/mx

          text.gsub!(reg){|match|
            _do_lists_callback($~)
          }
        else
          reg = /(?:(?<=\n)\n|\A\n?)
(?<whole_list>
 (
   (?<spaces>[ ]{0,3})
   (?<marker>#{marker_re})
   [ ]+
 )
 (?m:.+?)
 (
  \z
   |
     \n{2,}
     (?=\S)
     (?!
      [ ]*#{marker_re}[ ]+
     )
   |
  (?=
    \n
    \k<spaces>#{other_marker_re}[ ]+
  )
 )
)/mx
          text.gsub!(reg){|match|
            _do_lists_callback($~)
          }
        end

      end

      return text
    end

    private
    # @param [String] matches Match result of do_lists.
    # @return [String] Hashed list tag.
    def _do_lists_callback(matches)
      # Re-usable patterns to match list item bullets and number markers:
      marker_ul_re  = '[*+-]'
      marker_ol_re  = '\d+[\.]'
      marker_any_re = "(?:#{marker_ul_re}|#{marker_ol_re})"

      list = matches[:whole_list]
      list_type = (matches[:marker].match(/#{marker_ul_re}/) ? "ul" : "ol")

      marker_any_re = (list_type == "ul" ? marker_ul_re : marker_ol_re)

      list += "\n"
      result = process_list_items(list, marker_any_re)
      result = hash_block("<#{list_type}>\n#{result}</#{list_type}>")
      return "\n#{result}\n\n"
    end

    private
    #
    # Process the contents of a single ordered or unordered list, splitting it
    # into individual list items.
    #
    # @param [String] list_str Match result of _do_lists.
    # @param [String] marker_any_re Regexp pattern to match list markers.
    # @return [String] list items.
    def process_list_items(list_str, marker_any_re)
      # The $this->list_level global keeps track of when we're inside a list.
      # Each time we enter a list, we increment it; when we leave a list,
      # we decrement. If it's zero, we're not in a list anymore.
      #
      # We do this because when we're not inside a list, we want to treat
      # something like this:
      #
      #   I recommend upgrading to version
      #   8. Oops, now this line is treated
      #   as a sub-list.
      #
      # As a single paragraph, despite the fact that the second line starts
      # with a digit-period-space sequence.
      #
      # Whereas when we're inside a list (or sub-list), that line will be
      # treated as the start of a sub-list. What a kludge, huh? This is
      # an aspect of Markdown's syntax that's hard to parse perfectly
      # without resorting to mind-reading. Perhaps the solution is to
      # change the syntax rules such that sub-lists must start with a
      # starting cardinal number; e.g. "1." or "a.".

      @list_level += 1

      # trim trailing blank lines:
      list_str.gsub!(/\n{2,}\z/,"\n")

      list_str.gsub!(%r{
        (?<leading_line>\n)?
        (?<leading_space>^[ ]*)
        (?<marker_space>#{marker_any_re}
          (?:[ ]+|(?=\n)) # space only required if item is not empty
        )
        (?<text>(?m:.*?))
        (?:
          (?<tailing_blank_line>\n+(?=\n))  # tailing blank line = $5
          |
          \n
        )
        (?= \n* (\z | \k<leading_space> (#{marker_any_re}) (?:[ ]+|(?=\n))))
        }xm) {|match|
          _process_list_items_callback($~)
      }

      @list_level -= 1
      return list_str
    end

    private
    #
    # Process the contents of a single ordered or unordered list, splitting it
    # into individual list tags.
    #
    # @param [String] matches Match result of process_list_items.
    # @return [String] list items.
    def _process_list_items_callback(matches)
      item = matches[:text]
      leading_line = matches[:leading_line]
      leading_space = matches[:leading_space]
      marker_space = matches[:marker_space]
      tailing_blank_line = matches[:tailing_blank_line]

      if leading_line || tailing_blank_line || item.match(/\n{2,}/)
        # Replace marker with the appropriate whitespace indentation
        item = leading_space + (' ' * marker_space.size) + "\n" + item
        item = run_block_gamut(outdent(item) + "\n")
      else
        # Recursion for sub-lists:
        item = do_lists(outdent(item))
        item.gsub!(/\n+\Z/, '')
        item = run_span_gamut(item)
      end

      return "<li>" + item + "</li>\n"
    end

    private
    #
    # Process Markdown `<pre><code>` blocks.
    # 
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def do_code_blocks(text)
      text.gsub!(%r{
        (?:\n\n|\A\n?)
        (?<codeblock> # the code block -- one or more lines, starting with a space/tab
          (?>
          [ ]{#{tab_width}}  # Lines must start with a tab or a tab-width of spaces
          .*\n+
          )+
        )
        ((?=^[ ]{0,#{tab_width}}\S)|\Z) # Lookahead for non-space at line-start, or end of doc
        }x) {|match|
        _do_code_blocks_callback($~)
      }
      return text
    end

    private
    # @param [String] matches Match result of do_code_block.
    # @return [String] Hashed code block.
    def _do_code_blocks_callback(matches) 
      codeblock = matches[:codeblock]
  
      codeblock = outdent(codeblock)
      codeblock = escape_html_no_quotes(codeblock)
  
      # trim leading newlines and trailing newlines
      codeblock.gsub!(/\A\n+|\n+\z/, '')
      codeblock = "<pre><code>#{codeblock}\n</code></pre>"
      return "\n\n#{hash_block(codeblock)}\n\n"
    end

    private
    #
    # Create a code span markup for code. Called from handle_span_token.
    #
    # @param [String] text text to be transformed.
    # @return [String] Hashed code tag.
    def make_code_span(code)
      code = escape_html_no_quotes(code.strip)

      return hash_part("<code>#{code}</code>")
    end

    private
    #
    # Prepare regular expressions for searching emphasis tokens in any
    # context.
    #
    def prepare_italics_and_bold
      @em_relist.each do |em, em_re|
        @strong_relist.each do |strong, strong_re|
          # Construct list of allowed token expressions.
          token_relist = []
          if @em_strong_relist.key?(em + strong)
            token_relist.push(@em_strong_relist[em + strong])
          end
          token_relist.push(em_re)
          token_relist.push(strong_re)
  
          # Construct master expression from list.
          token_re = '(' + token_relist.join('|') + ')'
          @em_strong_prepared_relist[em + strong] = token_re
        end
      end 
    end

    private
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def do_italics_and_bold(text)
      token_stack = []
      text_stack = []
      em = ''
      strong = ''
      tree_char_em = false

      loop do
        #
        # Get prepared regular expression for seraching emphasis tokens
        # in current context.
        #
        token_re = @em_strong_prepared_relist[em + strong]

        #
        # Each loop iteration search for the next emphasis token. 
        # Each token is then passed to handle_span_token.
        #
        parts = text.split(/#{token_re}/, 2)
        text_stack[0] = text_stack[0] ? text_stack[0] + parts[0].to_s : parts[0].to_s
        token = parts[1]
        text = parts[2]

        if token.nil? || token.empty?
          # Reached end of text span: empty stack without emitting.
          # any more emphasis.
          while token_stack[0]
            text_stack[1] = text_stack[1] ? text_stack[1] + token_stack.shift : token_stack.shift
            text_stack[0] = text_stack[0] ? text_stack[0] + text_stack.shift : ext_stack.shift
          end
          break
        end

        token_len = token.size
        if tree_char_em
          # Reached closing marker while inside a three-char emphasis.
          if token_len == 3
            # Three-char closing marker, close em and strong.
            token_stack.shift
            span = text_stack.shift
            span = run_span_gamut(span)
            span = "<strong><em>#{span}</em></strong>"
            text_stack[0] = text_stack[0] ? text_stack[0] + hash_part(span) : hash_part(span)
            em = ''
            strong = ''
          else
            # Other closing marker: close one em or strong and
            # change current token state to match the other
            token_stack[0] = token[0].chr * (3 - token_len)
            tag = (token_len == 2 ? "strong" : "em")
            span = text_stack[0]
            span = run_span_gamut(span)
            span = "<#{tag}>#{span}</#{tag}>"
            text_stack[0] = hash_part(span)
            #{tag} = "" # tag stands for em or strong
          end
          tree_char_em = false
        elsif token_len == 3
          if em != ''
            # Reached closing marker for both em and strong.
            # Closing strong marker:
            for i in 0..1
              shifted_token = token_stack.shift
              tag = (shifted_token.to_s.size == 2 ? "strong" : "em")
              span = text_stack.shift
              span = run_span_gamut(span)
              span = "<#{tag}>#{span}</#{tag}>"
              text_stack[0] = text_stack[0] ? text_stack[0] + hash_part(span) : hash_part(span)
              #{tag} = "" # tag stands for em or strong
              i += 1
            end
          else
            # Reached opening three-char emphasis marker. Push on token 
            # stack; will be handled by the special condition above.
            em = token[0].chr
            strong = "#{em}#{em}"
            token_stack.unshift(token)
            text_stack.unshift('')
            tree_char_em = true
          end
        elsif token_len == 2
          if strong != ''
            # Unwind any dangling emphasis marker:
            if token_stack[0].to_s.size == 1
              text_stack[1] = text_stack[1] ? text_stack[1] + token_stack.shift : token_stack.shift
              text_stack[0] = text_stack[0] ? text_stack[0] + text_stack.shift : text_stack.shift
            end
            # Closing strong marker:
            token_stack.shift
            span = text_stack.shift
            span = run_span_gamut(span)
            span = "<strong>#{span}</strong>"
            text_stack[0] = text_stack[0] ? text_stack[0] + hash_part(span) : hash_part(span)
            strong = ''
          else
            token_stack.unshift(token)
            text_stack.unshift('')
            strong = token
          end
        else
          # Here token_len == 1
          if em != ""
            if token_stack[0].size == 1
              # Closing emphasis marker:
              token_stack.shift
              span = text_stack.shift
              span = run_span_gamut(span)
              span = "<em>#{span}</em>"
              text_stack[0] = text_stack[0] ? text_stack[0] + hash_part(span) : hash_part(span)
              em = ''
            else
              text_stack[0] = text_stack[0] ? text_stack[0] + token : token
            end
          else
            token_stack.unshift(token)
            text_stack.unshift('')
            em = token
          end
        end
      end
      return text_stack[0]
    end

    private
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def do_block_quotes(text)
      text = text.gsub(/
         ( # Wrap whole match in $1
          (?>
            ^[ ]*>[ ]?                        # ">" at the start of a line
            .+\n                                        # rest of the first line
            (.+\n)*                                        # subsequent consecutive lines
            \n*                                                # blanks
            )+
          )/x){|match|
        _do_block_quotes_callback($~)
      }
      return text
    end

    private
    # @param [String] matches Match result of do_block_quotes.
    # @return [String] Hashed blockqupte tag.
    def _do_block_quotes_callback(matches)
      bq = matches[1]
      # trim one level of quoting - trim whitespace-only lines
      bq.gsub!(/^[ ]*>[ ]?|^[ ]+$/m, '')
      bq = run_block_gamut(bq) # recurse

      bq.gsub!(/^/m, '  ')
      # These leading spaces cause problem with <pre> content, 
      # so we need to fix that:
      bq = bq.gsub(%r{(\s*<pre>.+?</pre>)}m) {|match|
        _do_block_quotes_callback2($~)
      }

      return "\n" + hash_block("<blockquote>\n#{bq}\n</blockquote>") + "\n\n"
    end

    private
    # @param [String] matches Match result of _do_block_quotes_callback. <pre> content.
    # @return [String] <pre> content without leading whitespaces.
    def _do_block_quotes_callback2(matches)
      pre = matches[1]
      pre.gsub!(/^  /m, '')
      return pre
    end

    private
    # @param [String] text string to process with html <p> tags
    # @return [String] Text after transformation.
    def form_paragraphs(text) #stab
      # Strip leading and trailing lines:
      text.gsub!(/\A\n+|\n+\z/, '')
      grafs = text.split(/\n{2,}/)
      grafs.delete_if {|item| item =~ /^\s*$/} # Delete empty elements

      #
      # Wrap <p> tags and unhashify HTML blocks
      #
      grafs.each_with_index do |value, key|
        if /^B\x1A[0-9]+B$/ !~ value
          value = run_span_gamut(value)
          value.gsub!(/\A([ ]*)/,'<p>')
          value += "</p>"
          grafs[key] = unhash(value)
        else
          # Is a block.
          # Modify elements of @grafs in-place..
          graf = value
          block = @html_hashes[graf]
          graf = block
          grafs[key] = graf
        end
      end

      return grafs.join("\n\n")
    end

    private
    #
    # Encode text for a double-quoted HTML attribute. This function
    # is *not* suitable for attributes enclosed in single quotes.
    #
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def encode_attribute(text)
      text = encode_amps_and_angles(text)
      text.gsub!(/\"/, '&quot;')
      return text
    end

    private
    #
    # Smart processing for ampersands and angle brackets that need to 
    # be encoded. Valid character entities are left alone unless the
    # no-entities mode is set.
    #
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def encode_amps_and_angles(text)
      if @no_entities
        text.gsub!(/\&/, '&amp;')
      else
        # Ampersand-encoding based entirely on Nat Irons's Amputator
        # MT plugin: <http://bumppo.net/projects/amputator/>
        text.gsub!(/&(?!#?[xX]?(?:[0-9a-fA-F]+|\w+);)/, '&amp;')
      end
      # Encode remaining <'s
      text.gsub!('<', '&lt;')

      return text
    end

    private
    # @param [String] text text to be transformed.
    # @return [String] Text after transformation.
    def do_auto_links(text)
      text.gsub!(%r{<((https?|ftp|dict):[^\'">\s]+)>}i){|match|
        _do_auto_links_url_callback($~)
      }

      # Email addresses: <address@domain.foo>
      text.gsub!(%r{
      <
      (?:mailto:)?
      (
        (?:
          [-!#\$%&\'*+/=?^_`.{|}~\w\\x80-\\xFF]+
        |
          ".*?"
        )
        \@
        (?:
          [-a-z0-9\\x80-\\xFF]+(\.[-a-z0-9\\x80-\\xFF]+)*\.[a-z]+
        |
          \[[\d.a-fA-F:]+\] # IPv4 & IPv6
        )
      )
      >
      }xi){|match|
        _do_auto_links_email_callback($~)
      }

      return text
    end

    private
    # @param [String] matches Match result of do_auto_links.
    # @return [String] Hashed <a> tag.
    def _do_auto_links_url_callback(matches)
      url = encode_attribute(matches[1])
      link = "<a href=\"#{url}\">#{url}</a>"
      return hash_part(link)
    end

    private
    # @param [String] matches Match result of do_auto_links. Mail address.
    # @return [String] Hashed <a> tag.
    def _do_auto_links_email_callback(matches)
      address = matches[1]
      link = encode_email_address(address)
      return hash_part(link)
    end

    private
    #
    #        Input: an email address, e.g. "foo@example.com"
    #
    #        Output: the email address as a mailto link, with each character
    #                of the address encoded as either a decimal or hex entity, in
    #                the hopes of foiling most address harvesting spam bots. E.g.:
    #
    #          <p><a href="&#109;&#x61;&#105;&#x6c;&#116;&#x6f;&#58;&#x66;o&#111;
    #        &#x40;&#101;&#x78;&#97;&#x6d;&#112;&#x6c;&#101;&#46;&#x63;&#111;
    #        &#x6d;">&#x66;o&#111;&#x40;&#101;&#x78;&#97;&#x6d;&#112;&#x6c;
    #        &#101;&#46;&#x63;&#111;&#x6d;</a></p>
    #
    #        Based by a filter by Matthew Wickline, posted to BBEdit-Talk.
    #   With some optimizations by Milian Wolff.
    #
    # @param [String] addr Match result of _do_auto_links_email_callback. Mail address.
    # @return [String] Encoded mail address.
    def encode_email_address(addr)
      addr = "mailto:" + addr
      chars = addr.split(/(?<!^)(?!$)/)
      seed = Zlib.crc32(addr).abs / addr.size  # Deterministic seed.

      chars.each_with_index do | char, key |
        ord = char.bytes.to_a[0].to_s
        # Ignore non-ascii chars.
        if ord.to_i < 128
          r = (seed * (1 + key)) % 100 # Pseudo-random function.
          # roughly 10% raw, 45% hex, 45% dec
          # '@' *must* be encoded. I insist.
          if r > 90 && char != '@'
            # do nothing
          elsif r < 45
            chars[key] = '&#x' + ord.to_i.to_s(16) + ';'
          else
            chars[key] = '&#' + ord + ';'
          end
        end
      end

      addr = chars.join
      text = chars.drop(7).join # text without `mailto:`
      addr = "<a href=\"#{addr}\">#{text}</a>";

      return addr
    end

    private
    #
    # Take the string str and parse it into tokens, hashing embeded HTML,
    # escaped characters and handling code spans.
    #
    # @param [String] str String to be Parsed.
    # @return [String] Text after transformation.
    def parse_span(str)
      output = ''

      # TODO: make regexp readable
      if @no_markup
        span_re = %r{(\\#{@escape_chars_re}|(?<![`\\])`+)}xm
      else
        span_re = %r{(\\#{@escape_chars_re}|(?<![`\\])`+|<!--    .*?     -->|<\?.*?\?> | <%.*?%>|<[!$]?[-a-zA-Z0-9:_]+(?>\s(?>[^"'>]+|"[^"]*"|'[^']*')*)?>|<[-a-zA-Z0-9:_]+\s*/>|</[-a-zA-Z0-9:_]+\s*>)}xm
      end

      loop do
        #
        # Each loop iteration seach for either the next tag, the next 
        # openning code span marker, or the next escaped character. 
        # Each token is then passed to handle_span_token.
        #
        parts = str.split(span_re, 2)

        # Create token from text preceding tag.
        if parts[0] != nil
          output += parts[0]
        end

        # Check if we reach the end.
        if parts[1]
          data = handle_span_token(parts[1], parts[2])
          output += data[:ret]
          str = data[:str]
        else
          break
        end
      end

      return output
    end

    private
    #
    # Handle token provided by parse_span by determining its nature and 
    # returning the corresponding value that should replace it.
    #
    # @param [String] token String to represent nature of str.
    # @param [String] str String to be transformed.
    # @return [Hash] :ret => transformed string. :str => remaining text.
    def handle_span_token(token, str)
      data = {
        :ret => "",
        :str => str,
      }

      case token[0]
        when "\\"
          data[:ret] = hash_part("&#" + token[1].bytes.to_a[0].to_s + ";")
          return data
        when "`"
          # Search for end marker in remaining text.
          token_esc = Regexp.escape(token).to_s
          re = /^(.*?[^`])#{token_esc}(?!`)(.*)$/sm
          if matches = str.match(re)
            data[:str] = matches[2]
            codespan = make_code_span(matches[1])
            data[:ret] = hash_part(codespan)
            return data
          end
          data[:ret] = token
          return data # return as text since no ending marker found.
        else
          data[:ret] = hash_part(token)
          return data
      end
    end

    private
    #
    # Remove one level of line-leading tabs or spaces
    #
    # @param [String] text Text to be transformed.
    # @return [String] Outdented text.
    def outdent(text)
      return text.gsub(/^(\t|[ ]{1,#{@tab_width.to_s}})/m, '')
    end

    private
    #
    # Replace tabs with the appropriate amount of space.
    #
    # @param [String] text Text to be transformed.
    # @return [String] Text after transformation.
    def detab(text)
      # For each line we separate the line in blocks delemited by
      # tab characters. Then we reconstruct every line by adding the 
      # appropriate number of space between each blocks.
  
      text = text.gsub(/^.*\t.*$/) {|match|
        _detab_callback($~)
      }

      return text
    end

    private
    # @param [String] matches Match result of detab. Single line.
    # @return [String] Line with white spaces that replace tabs.
    def _detab_callback(matches)
      line = matches[0]
  
      # Split in blocks.
      blocks = line.split("\t")
  
      # Add each blocks to the line.
      line = blocks[0]
      blocks.delete_at(0) # Do not add first block twice.
      blocks.each do |block|
        # Calculate amount of space, insert spaces, insert block.
        amount = @tab_width - line.size.modulo(@tab_width)
        line += " " * amount + block
      end   
      return line
    end

    private
    #
    # Swap back in all the tags hashed by _hash_html_blocks.
    #
    # @param [String] text Text to be transformed.
    # @return [String] Unhashed text.
    def unhash(text)
      return text.gsub(/(.)\x1A[0-9]+\1/) {|match|
        _unhash_callback($~)
      }

    end

    private
    #
    # Swap back in all the tags hashed by _hash_html_blocks.
    #
    # @param [String] matches Match result of unhash.
    # @return [String] Unhashed HTML block.
    def _unhash_callback(matches)
      return @html_hashes[matches[0]]
    end

    private 
    # 
    # Sort hash by value
    # Implementation of PHP asort function.
    #
    # @param [Hash] hash to sort.
    # @return [Hash] Sorted hash.
    def asort(hash)
      array = hash.sort_by{|key, value| value}.flatten
      return Hash[*array]
    end

    private
    # 
    # Escape &, < and > charactors.
    # " and ' are not replaced.
    #
    # @param [String] text Text to be transformed.
    # @param [String] Text after transformation.
    def escape_html_no_quotes(text)
      return text.gsub(
        /<|>|&/,
         "<" => "&lt;", ">" => "&gt;", "&" => "&amp;"
      )
    end
  end
end
