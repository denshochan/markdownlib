<?php
include_once './Markdown.php';

use \Michelf\Markdown;

#$my_text = "\nHere's a [link] [1] with an ampersand in the URL.\n\n[1]: http://example.com/?foo=1&bar=2";

#$my_html = Markdown::defaultTransform($my_text);

#$my_html;

$parser = new Markdown;
$my_text = \file_get_contents(__dir__."/spec/MarkdownTest_1.0.3/Temp/Horizontal rules.text");

print $parser->transform($my_text);

