# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'markdownlib/version'

Gem::Specification.new do |spec|
  spec.name          = "markdownlib"
  spec.version       = Markdownlib::VERSION
  spec.authors       = ["denchochannel"]
  spec.email         = ["info@denchochan.com"]
  spec.description   = %q{Pure Ruby Markdown library of similar capability to PHP Markdown Lib}
  spec.summary       = %q{Pure Ruby Markdown library of similar capability to PHP Markdown Lib}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rspec-expectations"
  spec.add_development_dependency "yard"
end
